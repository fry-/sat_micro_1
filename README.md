# Single And Team Micro map

Starcraft Broodwar / Remastered funmap.
Written in [LangUMS](https://github.com/LangUMS/langums)

## Gameplay

Buy units and upgrades to defeat different enemy waves.
Each round consists of a solo- and team-battle.
If you survive the solo-battle, you will participate in the team-battle.

## License

MIT License (see [LICENSE.md](LICENSE.md))