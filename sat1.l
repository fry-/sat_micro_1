#src sat1.scx

// SAT micro map by Fry-

// definitions
#define DEBUG true
#define FIVE_SECONDS 30

// globals
global current_level = 1;
global difficulty = 0; // 3 = easy, 2 = normal, 1 = hard
global timer = 0;
global p1_mineral_count = 0;
global p2_mineral_count = 0;
global p1_gas_count = 0;
global p1_energy_count = 50;
global patrol_order = false;
global step_upgrade = false;
global step_building = false;
global step_prefight = false;
global step_fight = false;
global step_pre_all = false;
global step_all = false;

// run the game
fn main() {
    center_view(AllPlayers, start_screen);
    print("""<04>Pick a difficulty level:
    <07>- Easy
    <11>- Normal
    <06>- Hard""");
    
    if (DEBUG == true) {
        set_vision(Force1,Player8,true);
        move(AllUnits, Player1, All, start_screen, easy);
    }
    // give full vision to enemy
    for <player> in (Player1, Player2, Player3, Player4, Player5, Player6, Player7) {
        set_vision(Player8,player,true);
    }

    round(1);
    clear_buffered_events();

    while(true) {
        teleport(Player1, p1_energy_count, p1_build, p1_spawn);
        fight_phase();
        poll_events();
    }
    
}

accumulate(Player1, Exactly, 0, Ore) => {
    if (DEBUG == true) {
        set_resource(Force1, OreAndGas, 500);
    }
}

// pick difficulty at game start
bring(Player1, AtLeast, 1, AllUnits, easy) => {
    difficulty = 18;
    print("<07>EASY mode");
    kill(AllUnits, Player1, 1, easy);
    kill(AllUnits, Player8, 1, normal);
    kill(AllUnits, Player8, 1, hard);
    kill(AllUnits, Player12, All, normal_letters);
    kill(AllUnits, Player12, All, hard_letters);
    set_resource(Force1, Ore, difficulty);
    prebuild_phase();
}
bring(Player1, AtLeast, 1, AllUnits, normal) => {
    difficulty = 12;
    print("<11>NORMAL mode");
    kill(AllUnits, Player1, 1, normal);
    kill(AllUnits, Player8, 1, easy);
    kill(AllUnits, Player8, 1, hard);
    kill(AllUnits, Player12, All, easy_letters);
    kill(AllUnits, Player12, All, hard_letters);
    set_resource(Force1, Ore, difficulty);
}
bring(Player1, AtLeast, 1, AllUnits, hard) => {
    difficulty = 6;
    print("<06>HARD mode");
    kill(AllUnits, Player1, 1, hard);
    kill(AllUnits, Player8, 1, easy);
    kill(AllUnits, Player8, 1, normal);
    kill(AllUnits, Player12, All, easy_letters);
    kill(AllUnits, Player12, All, normal_letters);
    set_resource(Force1, Ore, difficulty);
}

// prevent lifting of buildings
bring(Player1, AtLeast, 1, Buildings ,p1_starport) => {
    kill(Buildings, Player1, 1, p1_starport);
    spawn(TerranStarport, Player1, 1, p1_starport, invincible_unit);
}
bring(Player1, AtLeast, 1, Buildings ,p1_factory) => {
    kill(Buildings, Player1, 1, p1_factory);
    spawn(TerranFactory, Player1, 1, p1_factory, invincible_unit);
}
bring(Player1, AtLeast, 1, Buildings ,p1_barracks) => {
    kill(Buildings, Player1, 1, p1_barracks);
    spawn(TerranBarracks, Player1, 1, p1_barracks, invincible_unit);
}
bring(Player1, AtLeast, 1, Buildings ,p1_science) => {
    kill(Buildings, Player1, 1, p1_science);
    spawn(TerranScienceFacility, Player1, 1, p1_science, invincible_unit);
}
bring(Player1, AtLeast, 1, Buildings ,p1_bay) => {
    kill(Buildings, Player1, 1, p1_bay);
    spawn(TerranEngineeringBay, Player1, 1, p1_bay, invincible_unit);
}

// spawn extra larva
commands(Player1, AtMost, 200, ZergLarva) => {
    give(ZergLarva, Player8, Player1, 12, larva_farm);
    move(ZergLarva, Player1, All, larva_farm, p1_larva);
}


// teleport units
fn teleport<player, from_location, to_location>(player, energy_value, from_location, to_location) {
    modify(Men, player, All, Energy, 50, from_location);
    modify(Men, player, All, Hangar, 10, from_location);
    move(Men, player, All, from_location, to_location);
}

// spawn enemies 
fn spawn_enemies<unit_type, player, location> (unit_type, player, amount, location) {
    if (is_present(player)) {
        spawn(unit_type, Player8, amount, location);
        modify(unit_type, Player8, All, Energy, 100, location);
        modify(unit_type, Player8, All, Hangar, 10, location);
    }
}

// all rounds
fn round(number) {
    if (number == 1) {
        spawn_enemies(ZergZergling, Player1, 6, c1_spawn);
        spawn_enemies(ZergZergling, Player8, 2, c8_spawn_all);
        return;
    }
    if (number == 2) {
        spawn_enemies(ProtossZealot, Player1, 10, c1_spawn);
        spawn_enemies(ProtossProbe, Player1, 10, c1_spawn);
        spawn_enemies(ProtossReaver, Player8, 1, c8_spawn_all);
        return;
    }
}

// // phases

// PAY
fn pay_players() {
    give_ore(Player1, p1_mineral_count);
    give_ore(Player2, p2_mineral_count);
}
fn give_ore<player>(player, variable) {
    var factor = difficulty + variable;
    set_resource(player, Ore, factor * current_level);
}
bring(Force1, AtMost, 0, TerranCivilian, p1_upgrade),
bring(Force1, AtMost, 0, TerranCivilian, p1_battleground) => {
    if (step_upgrade == true) {
        step_upgrade = false;
        print("PAYDAY");
        print("PAYDAY");
        print("PAYDAY");
        print("PAYDAY");
        pay_players();
    }
}

// UPGRADE
fn upgrade_phase() {
    current_level++;
    round(current_level);
    move_to_upgrade(Player1, p1_upgrade);
    set_countdown(5);
    print("Upgrade phase");
    step_upgrade = true;
}
fn move_to_upgrade<player, location>(player, location) {
    move(TerranCivilian, player, All, AnyLocation, location);
    center_view(player, location);
}
bring(Player1, Exactly, 1, TerranCivilian, p1_minerals) => {
    if (step_upgrade == true) {
        p1_mineral_count++;
        move(TerranCivilian, Player1, 1, p1_minerals, p1_reset);
    }
}
bring(Player1, Exactly, 1, TerranCivilian, p1_gas) => {
    if (step_upgrade == true) {
        p1_gas_count++;
        move(TerranCivilian, Player1, 1, p1_gas, p1_reset);
    }
}
bring(Player1, Exactly, 1, TerranCivilian, p1_energy) => {
    if (step_upgrade == true) {
        p1_energy_count++;
        move(TerranCivilian, Player1, 1, p1_energy, p1_reset);
    }
}

// BUILD
fn prebuild_phase() {
    center_view(Player1, p1_factory);
    if (DEBUG == true) {
        set_countdown(6);
    } else {
        set_countdown(40);
    }
    step_building = true;
}

// FIGHT
fn fight_phase() {
    if (step_all == true) {
        move_loc(AllUnits, Force1, all_battleground, all_attack);
        if (patrol_order == true) {
            order(AllUnits, Player8, Patrol, all_battleground, all_attack);
            print("all attack trigger");
            patrol_order = false;
        }
    }
    if (step_fight == true) {
        move_loc(AllUnits, Player1, p1_battleground, p1_attack);
        if (patrol_order == true) {
            order(AllUnits, Player8, Patrol, p1_battleground, p1_attack);
            print("trigger attack move");
            patrol_order = false;
        }
    }
}
bring(Player1, AtLeast, 1, AllUnits, p1_battleground),
bring(Player8, Exactly, 0, AllUnits, p1_battleground) => { // p1_ finished solo round
    if (step_fight == true) {
        move(AllUnits, Player1, All, p1_battleground, p1_spawn_all);
    }
}
bring(Player8, AtLeast, 1, AllUnits, p1_battleground),
bring(Player1, Exactly, 0, AllUnits, p1_battleground) => { // p1_ lost solo round
    if (step_fight == true) {
        move(AllUnits, Player8, All, p1_battleground, c1_spawn_all);
    }
}
//////////////////////// TODO
bring(Player8, Exactly, 0, AllUnits, p1_battleground) => { // all players finished their battle, ADD MORE BATTLEGROUND HERE!!!!
    if (step_fight == true) {
        step_fight = false;
        if (DEBUG == true) {
            set_countdown(2);
        } else {
            set_countdown(10);
        }
        step_pre_all = true;
    }
}
////////////////////////
bring(Player8, Exactly, 0, AllUnits, all_battleground) => { // round finished, start next round!
    if (step_all == true) {
        step_all = false;
        kill(AllUnits, Force1, All, all_battleground);
        print("Round 1 finished!");
        upgrade_phase();
    }
}
bring(Force1, Exactly, 0, AllUnits, all_battleground) => { // humans lost!
    if (step_all == true) {
        step_all = false;
        print("you got defeated");
        // end(Force1, Defeat);
    }
}

// update patrol order
value(timer, AtMost, FIVE_SECONDS) => {
    if (step_fight == true || step_all == true) {
        timer++;
        if (timer == FIVE_SECONDS) {
            reset_patrol();
        }
    }
}
fn reset_patrol() {
    patrol_order = true;
    timer = 0;
}

// countdown specific triggers (upgrade-, build-, prefight-, all-prefight- phase)
countdown(Exactly,1) => {
    if (step_pre_all == true) {
        // sleep(1000);
        print("pre-all ended");
        step_pre_all = false;
        step_all = true;
        reset_patrol();
    }
    if (step_prefight == true){
        // sleep(1000);
        print("prefight ended");
        step_prefight = false;
        step_fight = true;
        reset_patrol();
    }
    if (step_building == true) {
        // sleep(1000);
        set_resource(Player1, Ore, 0);
        center_view(Player1, p1_spawn);
        step_prefight = true;
        if (DEBUG == true) {
            set_countdown(2);
        } else {
            set_countdown(10);
        }
        print("<04>Prepare for the battle!");
        step_building = false;
    }
    if (step_upgrade == true) {
        move(TerranCivilian, Player1, All, p1_upgrade, p1_minerals);
        print("Civilian force-moved");
    }
}
// player didn't buy an upgrade, default to mineral upgrade
countdown(Exactly, 1),
bring(Player1, Exactly, 1, TerranCivilian, p1_upgrade) => {
    move(TerranCivilian, Player1, All, p1_upgrade, p1_minerals);
}

elapsed_time(AtLeast, 0) => {
    if (step_pre_all == true) {
        move(AllUnits, Force1, All, all_deadzone, p2_spawn_all);
        order(AllUnits, Player8, Attack, all_battleground, all_move);
    }
    if (step_prefight == true) {
        move(AllUnits, Player1, All, p1_deadzone, p1_move);
        order(AllUnits, Player8, Attack, c1_spawn, c1_move);
    }
    if (step_building == true) {
        order(AllUnits, Player1, Attack, p1_spawn, p1_move);
        set_resource(Player1, Gas, p1_gas_count);
    }
}

// ban player
for <player> in (Player2, Player3, Player4, Player5, Player6, Player7) {
    deaths(player, AtLeast, 1, ZergCreepColony) => {
        end(player,Defeat);
    }
}

// unit type declaration
unit invincible_unit {
    Invincible = true
}